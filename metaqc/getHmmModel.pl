#!/usr/bin/env perl

use strict;
use Getopt::Long;
use Pod::Usage;

my $acc;
my $name;
my $help;
my $man;
GetOptions(
    'acc=s' => \$acc,
    'name=s' => \$name,
    'help|?' => \$help,
    'man' => \$man
) or pod2usage(2);
pod2usage(1) if $help;
pod2usage(-exitstatus => 0, -verbose => 2) if $man;

die "Usage: $0 [--acc <id>|--name <regex>] Rfam.cm > outfile.cm\n" unless $ARGV[0];

unless ( $acc or $name )
{
	die("--acc or --name required\n");
}

open FH,"$ARGV[0]" || die "$!\n";

my $rec='';
my $output=0;
my $gene_name;
while (<FH>)
{
	$rec .= $_;
	if (/^\/\/$/)
	{
		if ( $output )
		{
			print "$rec\n";
			warn("FOUND $gene_name\n");
		}
		$rec = '';
		$output = '';
	}

	chomp;
	if ($name and /^NAME\s+(\S+)/){
		$gene_name = $1;
		$output = 1 if ($gene_name =~ /$name/);
	} elsif ( $acc and /^ACC\s+(\S+)/){
		$output = 1 if ($gene_name =~ /$acc/);
	}
}
