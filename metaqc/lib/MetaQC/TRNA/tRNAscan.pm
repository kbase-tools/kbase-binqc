=pod

=head1 NAME

MetaQC::TRNA::tRNAscan

=head1 DESCRIPTION

Run tRNAscan-SE, filter results, save summary in object for querying.  While tRNAscan results can be provided, it is not necessary to run tRNAscan on every contig.  As tRNAscan-SE is very slow, this class supports running tRNAscan as needed only, with results added to the growing tRnascan results file.

=head1 METHODS

=over 5

=cut

package MetaQC::TRNA::tRNAscan;

use strict;
use JSON;
use File::Path qw(make_path remove_tree);
use File::Slurp;
use File::Which;
use File::Copy;
use Parallel::ForkManager;

=item new

Constructor requires output folder and contigs FASTA as 'dir' and 'contigs' params respectively.  tRnascan-SE output may also be provided as 'input' parameter; if all contigs were searched then the 'all' flag should also be set, otherwise contigs without hits will be re-searched.

=cut

sub new
{
	my ($class, $config, $dir, $contigsFile, $threads, $infile) = @_;
	defined($config) or die("Config required\n");
	$dir or die("Dir required\n");
	$contigsFile or die("Contigs file required\n");
	$threads or die("Threads required\n");
	exists($config->{tRNA}->{highMinTRnas}) or die("Config missing tRNA/highMinTRnas\n");
	which('tRNAscan-SE') or die("tRNAscan-SE executable not found\n");

	-d $dir or make_path($dir) or die("Unable to mkdir $dir: $!\n");
	my $tRnascanFile = "$dir/tRNA.tsv";
	my $tmpdir = "$dir/tRNAscan";

	my $this = 
	{
		config => $config,
		dir => $dir,
		tmpdir => $tmpdir,
		contigsFile => $contigsFile,
		tRnascanFile => $tRnascanFile,
		threads => $threads,
		tRNA => {},
		infile => $infile
	};
	bless $this, $class;

	# LOAD ANY EXISTING RESULTS
	$infile and $this->importTrnascan($infile);
	$this->load;
	return $this;
}

=item load

Load tRNA summaries from file

=cut

sub load
{
	my ($this) = @_;
	my %results;
	my $tRnascanFile = $this->{tRnascanFile};
	if ( -e $tRnascanFile )
	{
		print "Reading $tRnascanFile\n";
		open(my $in, '<', $tRnascanFile) or die($!);
		while (<$in>)
		{
			chomp;
			my ($contigId, @row) = split(/\t/);
			if ( exists($results{$contigId}) )
			{
				push @{$results{$contigId}}, \@row;
			} else
			{
				$results{$contigId} = [ \@row ];
			}
		}
		close($in);
		print "There are ", scalar(keys %results), " tRNA annotations\n";
	}
	$this->{tRNA} = \%results;

	# CHECK FOR TRNASCAN-SE TEMP OUTFILES THAT HAVEN'T BEEN IMPORTED YET
	my $tmpdir = $this->{tmpdir};
	if ( -d $tmpdir )
	{
		opendir(DIR, $tmpdir) or die($!);
		my @files = grep {/\.tRNAscan\.txt$/} readdir(DIR);
		closedir(DIR);

		foreach my $file (@files)
		{
			$file =~ /^(.+)\.tRNAscan\.txt$/ or next;
			my $fastaFile = "$tmpdir/$1.fna";
			-e $fastaFile or next;
			my $txtFile = "$tmpdir/$file";
			$this->importTrnascan($txtFile, $fastaFile);
			unlink($txtFile, $fastaFile);
		}
		rmdir($tmpdir);
	}
}

=item countNumTRnas

Given list of contig IDs, return number of different tRNAs.

=cut

sub countNumTRnas
{
	my ($this, $contigIds) = @_;
	my %tRNA;
	foreach my $contigId (@$contigIds)
	{
		$this->{infile} and !exists($this->{tRNA}->{$contigId}) and next;
		exists($this->{tRNA}->{$contigId}) or return('?');
		foreach my $row ( @{$this->{tRNA}->{$contigId}} )
		{
			@$row or next;
			my $aa = $row->[3];
			$aa =~ /^\w{3,5}$/ or warn("Invalid tRNAscan-SE AA \"$aa\" for $contigId\n");
			#$aa =~ /^(Opal|Amber|Ochre)$/i and $aa = 'Stop'; # TODO SIDE-COUNTS FOR MET AND STOPS?
			$tRNA{$aa} += 1;
		}
	}
	return scalar(keys %tRNA);
}

=item mostlyComplete

Given list of contig IDs, return true if has the minimum number of different tRNAs specified in the config file.

=cut

sub mostlyComplete
{
	my ($this, $contigIds) = @_;
	my $numTRnas = $this->countNumTRnas($contigIds);
	my $result = 0;
	if ( $numTRnas ne '?' and $numTRnas >= $this->{config}->{tRNA}->{highMinTRnas} ) { $result=1 }
	return wantarray ? ($result, $numTRnas) : $result;
}

=item search

Search contigs for tRNAs using tRNAscan-SE.  Requires contig ID and domain for models to use.

=cut

sub search
{
	my ($this, $contigIds, $domain) = @_;

	# IDENTIFY CONTIGS WHICH HAVEN'T BEEN PREVIOUSLY PROCESSED
	my %contigs;
	foreach my $contigId (@$contigIds)
	{
		exists($this->{tRNA}->{$contigId}) and next;
		$contigs{$contigId} = {};
	}
	my $numTodo = scalar(keys %contigs);
	$numTodo or return;
	print "Running tRNAscan-SE ($domain) on $numTodo contigs\n";

	# MAKE FASTA FILES, ONE PER CONTIG.
	# I ASSUME THE CONTIGS FILE IS ORDERED BY DECREASING CONTIG SIZE (AS IS USUALLY THE CASE),
	# WHICH RESULTS IN TASKS BEING RUN IN AN EFFICIENT ORDER (TO MAXIMIZE THREAD USAGE)
	-d $this->{tmpdir} or make_path($this->{tmpdir});
	my @infiles;
	my @outfiles;
	open(my $in, '<', $this->{contigsFile}) or die($!);
	my $out;
	while (<$in>)
	{
		if (/^>(\S+)/)
		{
			my $contigId = $1;
			defined($out) and close($out);
			scalar(keys %contigs) or last;
			if ( exists($contigs{$contigId}) )
			{
				my $i = scalar(@infiles);
				my $infile = $infiles[$i] = "$this->{tmpdir}/$i.contig.fna";
				my $outfile = $outfiles[$i] = "$this->{tmpdir}/$i.tRNAscan.txt";
				-f $outfile and unlink($outfile);
				open($out, '>', $infile) or die("Unable to write tmpfile, $infile: $!\n");
				delete($contigs{$contigId});
			} else
			{
				$out = undef;
			}
		}
		defined($out) and print $out $_;
	}
	close($in);
	defined($out) and close($out);

	# RUN TRNASCAN-SE, ONE CONTIG PER THREAD, ORDERED BY DECREASING SIZE
	my $pm = new Parallel::ForkManager($this->{threads});
	for (my $i=0; $i<@infiles; ++$i)
	{
		$pm->start and next;
		my $infile = $infiles[$i];
		my $outfile = $outfiles[$i];
		-f $outfile and unlink($outfile);

		my $tmpfile = "$outfile.tmp";
		-f $tmpfile and unlink($tmpfile);
		my $cmd;
		if ( $domain eq 'archaea' )
		{
			$cmd="tRNAscan-SE -A -C -D -q -o \"$tmpfile\" \"$infile\"";
		} elsif ( $domain eq 'bacteria' )
		{
			$cmd="tRNAscan-SE -B -C -D -q -o \"$tmpfile\" \"$infile\"";
		} else
		{
			die("Invalid domain: $domain\n");
		}
		system($cmd) == 0 or die("ERROR: tRNAscan-SE FAILED: $!\n$cmd\n");
		move($tmpfile, $outfile);
		$pm->finish;
	}
	$pm->wait_all_children;

	# READ TRNASCAN-SE RESULTS
	for (my $i=0; $i<@infiles; ++$i)
	{
		$this->importTrnascan($outfiles[$i], $infiles[$i]);
		unlink($infiles[$i], $outfiles[$i]);
	}

	# WRITE TRNA ANNOTATION FILE
	my $tRnascanFile = $this->{tRnascanFile};
	print "Writing tRNA annotations to $tRnascanFile\n";
	open(my $out, '>', $tRnascanFile) or die("Unable to write $tRnascanFile: $!\n");
	foreach my $contigId ( sort keys %{$this->{tRNA}} )
	{
		my $ar = $this->{tRNA}->{$contigId};
		if ( defined($ar) and scalar(@$ar) )
		{
			foreach my $row ( @$ar )
			{
				print $out join("\t", $contigId, @$row), "\n";
			}
		} else
		{
			print $out $contigId, "\n";
		}
	}
	close($out);
}

=item importTrnascan

Read tRNAscan-SE results and save results.  The fasta file is also used to create blank entries for all contigs searched (i.e. to prevent them from being searched again in the future)

=cut

sub importTrnascan
{
	my ($this, $infile, $fastaFile) = @_;
	unless ( $infile and -f $infile )
	{
		die("tRNAscan-SE output required\n");
	}
	print "Importing tRNAscan-SE results: $infile\n";

	# CREATE EMPTY RECORDS FOR ALL CONTIGS THAT WERE SEARCHED
	if ( $fastaFile )
	{
		open(my $in, '<', $fastaFile) or die("Unable to read $fastaFile: $!\n");
		while (<$in>)
		{
			chomp;
			/^>(\S+)/ or next;
			my $contigId = $1;
			exists($this->{tRNA}->{$contigId}) or $this->{tRNA}->{$contigId} = [];
		}
		close($in);
	}

	# LOAD RESULTS
	open(my $in, '<', $infile) or die("Unable to read $infile: $!\n");
	while (<$in>) { last if /^\-+/ }
	while (<$in>)
	{
		chomp;
		s/\s+/\t/g;
		my ($contigId, @row) = split(/\t/);
		if ( exists($this->{tRNA}->{$contigId}) )
		{
			push @{$this->{tRNA}->{$contigId}}, \@row;
		} else
		{
			$this->{tRNA}->{$contigId} = [ \@row ];
		}
	}
	close($in);
}

1;

=back

=head1 AUTHOR

Edward Kirton (ESKirton@LBL.gov)

=head1 COPYRIGHT/LICENSE

Copyright 2017 US DOE Joint Genome Institute.  Use freely under the same terms as Perl itself.

=head1 AUSPICE STATEMENT

The work conducted by the U.S. Department of Energy Joint Genome Institute is supported by the Office of Science of the U.S. Department of Energy under Contract No. DE-AC02-05CH11231.

=cut
