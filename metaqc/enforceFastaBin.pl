#!/usr/bin/env perl
use strict;
use warnings;

# Grab the fasta files and suffix
my $dir = $ARGV[0];
my $concatenated_contigs = $ARGV[1];

die "Usage: $0 <bins dir> <concatenated contigs output>\n" unless $dir && $concatenated_contigs;
die "Failed to find dir: $dir\n" unless -d $dir;

my @fasta;
my $suffix;
my @fasta_fa = glob("$dir/*.fa");
my @fasta_fasta = glob("$dir/*.fasta");
my @fasta_fna = glob("$dir/*fna");
my $i=0;
if (@fasta_fa){
    @fasta = @fasta_fa;
    $suffix = 'fa';
    $i++;
}
if(@fasta_fasta){
    @fasta = @fasta_fasta;
    $suffix = 'fasta';
    $i++;
}
if(@fasta_fna){
    @fasta = @fasta_fna;
    $suffix = 'fna';
    $i++;
}


die "Failed to find any bin fasta files. They must end in [fa|fasta|fna]\n"
if ($i == 0);

die "There seems to be fasta files using different suffixes.  Please use only one type of suffix [fa|fasta|fna]\n"
if ($i > 1);

`cat @fasta > $concatenated_contigs`;
print "wrote concatenated contigs file to $concatenated_contigs\n";