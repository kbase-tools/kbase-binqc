FROM kbase/sdkbase2:latest
MAINTAINER KBase Developer

COPY ./ /kb/module
RUN mkdir -p /kb/module/work
RUN chmod -R a+rw /kb/module

WORKDIR /kb/module

RUN make all

# dependency versions used at time of development
# pplacer==1.1.alpha19
# infernal==1.1 (cmsearch)
# prodigal==2.6.3
# hmmer==3.1b1
# checkm==1.0.11
# trnascan-se==1.3.1
# refinem==

#
# Install conda dependent trnascan-se and refinem
#
RUN wget https://repo.continuum.io/miniconda/Miniconda2-latest-Linux-x86_64.sh && \
    bash ./Miniconda2-latest-Linux-x86_64.sh -b

ENV PATH=$PATH:/root/miniconda2/bin
RUN conda install -y -c bioconda trnascan-se
RUN conda install -y -c bioconda bbmap

# install refineM (this has issues because instaling with pip
# makes refinem not see pysam libs that exist in miniconda/pkgs
#RUN conda install -y -c bioconda krona
#RUN ktUpdateTaxonomy.sh /root/miniconda2/opt/krona/taxonomy
#RUN pip install refinem

# all PERL modules and MetaQC pm's for metaQc.pl
ADD metaqc/lib/ /kb/deployment/lib/

# add checkm and dependencies to PATH
ENV CHECKM_VERSION=1.0.11

#
# Install apt-get dependencies
#
RUN apt-get update && \
    apt-get install -y hmmer prodigal unzip infernal

#
# Install pplacer
#
RUN wget "https://github.com/matsen/pplacer/releases/download/v1.1.alpha19/pplacer-linux-v1.1.alpha19.zip" && \
    unzip pplacer-linux-v1.1.alpha19.zip && \ 
    rm -f pplacer-linux-v1.1.alpha19.zip && \
    mv pplacer-Linux-v1.1.alpha19 /kb/deployment/bin/pplacer

ENV PATH "$PATH:/kb/deployment/bin/pplacer"
ENV PATH "$PATH:/kb/module/metaqc"

#
# Install checkm
#
RUN conda install -y -c bioconda pysam==0.15.0
RUN apt-get install -y libbz2-dev liblzma-dev
RUN pip install checkm-genome==1.0.8 && \
    cp -R /usr/local/bin/checkm /kb/deployment/bin/CheckMBin

# For checkm-genome required data
RUN mkdir /data && \
    mv /usr/local/lib/python2.7/dist-packages/checkm/DATA_CONFIG /usr/local/lib/python2.7/dist-packages/checkm/DATA_CONFIG.orig && \
    touch /data/DATA_CONFIG && \
    cp /usr/local/lib/python2.7/dist-packages/checkm/DATA_CONFIG.orig /data/DATA_CONFIG && \
    cp /data/DATA_CONFIG /usr/local/lib/python2.7/dist-packages/checkm/DATA_CONFIG

RUN mkdir -p /data/checkm_data
RUN echo /data/checkm_data | checkm data setRoot /data/checkm_data

ENTRYPOINT [ "./scripts/entrypoint.sh" ]
ENV PERL5LIB "/kb/module/metaqc/lib:/kb/module/metaqc/lib/tRNAscan-SE:$PERL5LIB"
CMD [ ]
