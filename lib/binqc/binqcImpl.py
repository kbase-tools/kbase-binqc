# -*- coding: utf-8 -*-
#BEGIN_HEADER
# The header block is where all import statments should live
import os
import json

from Utils.binqcUtil import binqc_runner

#END_HEADER


class binqc:
    '''
    Module Name:
    binqc

    Module Description:
    A KBase module: binqc
    This module contains one small method - bin_qc.
    '''

    ######## WARNING FOR GEVENT USERS ####### noqa
    # Since asynchronous IO can lead to methods - even the same method -
    # interrupting each other, you must be *very* careful when using global
    # state. A method could easily clobber the state set by another while
    # the latter method is running.
    ######################################### noqa
    VERSION = "1.0.13"
    GIT_URL = "https://gitlab.com/kbase-tools/kbase-binqc"
    GIT_COMMIT_HASH = ""

    #BEGIN_CLASS_HEADER
    # Class variables and functions can be defined in this block
    #END_CLASS_HEADER
    
   
    # config contains contents of config file in a hash or None if it couldn't
    # be found
    def __init__(self, config):
        #BEGIN_CONSTRUCTOR
        # Any configuration parameters that are important should be parsed and
        # saved in the constructor.
        self.config = config
        self.config['SDK_CALLBACK_URL'] = os.environ['SDK_CALLBACK_URL']
        #END_CONSTRUCTOR
    
    
    def bin_qc(self, ctx, params):
        """
        The actual function is declared using 'funcdef' to specify the name
        and input/return arguments to the function.  For all typical KBase
        Apps that run in the Narrative, your function should have the 
        'authentication required' modifier.
        :param params: instance of type "BinQCParams" (A 'typedef' can also
           be used to define compound or container objects, like lists, maps,
           and structures.  The standard KBase convention is to use
           structures, as shown here, to define the input and output of your
           function.  Here the input is a reference to the binned contigs
           data object and a workspace to save output. To define lists and
           maps, use a syntax similar to C++ templates to indicate the type
           contained in the list or map.  For example: list <string>
           list_of_strings; mapping <string, int> map_of_ints;) -> structure:
           parameter "binned_contig_obj_ref" of type "obj_ref" (An X/Y/Z
           style reference), parameter "workspace_name" of String
        :returns: instance of type "BinQCResults" (Here is the definition of
           the output of the function.  The output can be used by other SDK
           modules which call your code, or the output visualizations in the
           Narrative.  'report_name' and 'report_ref' are special output
           fields- if defined, the Narrative can automatically render your
           Report.) -> structure: parameter "report_name" of String,
           parameter "report_ref" of String
        """
        # ctx is the context object
        # return variables are: output
        #BEGIN bin_qc
        print '--->\nRunning binqc.bin_qc\nparams:'
        print json.dumps(params, indent=1)

        for key, value in params.iteritems():
            if isinstance(value, basestring):
                params[key] = value.strip()
        
        
        runner = binqc_runner(self.config, ctx)
        output = runner.bin_qc_util(params)
        #END bin_qc
        
        # At some point might do deeper type checking...
        if not isinstance(output, dict):
            raise ValueError('Method bin_qc return value ' +
                             'output is not type dict as required.')
        # return the results
        return [output]

    
    def status(self, ctx):
        #BEGIN_STATUS
        returnVal = {'state': "OK",
                     'message': "",
                     'version': self.VERSION,
                     'git_url': self.GIT_URL,
                     'git_commit_hash': self.GIT_COMMIT_HASH}
        #END_STATUS
        return [returnVal]
