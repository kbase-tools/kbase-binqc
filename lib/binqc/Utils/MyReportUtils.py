import json
import os
import uuid
import errno
import subprocess
import sys
import time
import zipfile
from pprint import pprint
from KBaseReport.KBaseReportClient import KBaseReport

def log(message, prefix_newline=False):
    """Logging function, provides a hook to suppress or redirect log messages."""
    print(('\n' if prefix_newline else '') + '{0:.2f}'.format(time.time()) + ': ' + str(message))
    
class CreateMyReport:
    '''
        Takes as input
    '''
    BINS='Bins'
    
    def __init__(self, config, ctx):
        # Any configuration parameters that are important should be parsed and
        # saved in the constructor.
        self.ctx = ctx
        self.config = config
        self.scratch = config['scratch']
        self.callback_url = os.environ['SDK_CALLBACK_URL']
        
    def _validate_run_binqc_params(self, params):
        """
        _validate_run_binqc_params:
                validates params passed to bin_qc_util method

        """
        log('Start validating bin_qc_util params')

        # check for required parameters
        for p in ['binned_contig_obj_ref', 'workspace_name']:
            if p not in params:
                raise ValueError('"{}" parameter is required, but missing'.format(p))
     
    def _mkdir_p(self, path):
        """
        _mkdir_p: make directory for given path
        """
        if not path:
            return
        try:
            os.makedirs(path)
        except OSError as exc:
            if exc.errno == errno.EEXIST and os.path.isdir(path):
                pass
            else:
                raise
    
    def _generate_output_file_list(self, result_directory):
        """
        _generate_output_file_list: zip result files and generate file_links for report
        """
        log('Start packing result files')
        output_files = list()

        output_directory = os.path.join(self.scratch, str(uuid.uuid4()))
        self._mkdir_p(output_directory)
        result_file = os.path.join(output_directory, 'binqc_result.zip')
        report_summary=''

        with zipfile.ZipFile(result_file, 'w',
                             zipfile.ZIP_DEFLATED,
                             allowZip64=True) as zip_file:
            
            # grab all files we want to zip
            for dirname, subdirs, files in os.walk(result_directory):
                baseDir=os.path.basename(dirname)
                for file in files:
                    if file == 'rRNA.tsv':
                        source = os.path.join(dirname,file)
                        dest   = os.path.join(baseDir,file)
                        print('writing to zipfile: {}'.format(dest))
                        zip_file.write(source, dest)
                    if file == 'summary.tsv':
                        report_summary=os.path.join(dirname, file)
                        source = os.path.join(dirname,file)
                        dest   = os.path.join(baseDir,file)
                        print('writing to zipfile: {}'.format(dest))
                        zip_file.write(source, dest)

        output_files.append({'path': result_file,
                             'name': os.path.basename(result_file),
                             'label': os.path.basename(result_file),
                             'description': 'Files generated by binQc App'})
        
        if not os.path.exists(report_summary):
            log('final results summary file not found: {}'.format(report_summary))
            sys.exit()
        
        return [output_files,report_summary]

    def _generate_html_report(self, report_summary):
        """
        _generate_html_report: generate html summary report
        """

        log('Start generating html report')
        html_report = list()

        output_directory = os.path.join(self.scratch, str(uuid.uuid4()))
        self._mkdir_p(output_directory)
        result_file_path = os.path.join(output_directory, 'report.html')

        # get summary data from existing assembly object and bins_objects
        TABLE_CONTENT_HERE=''

        summary_list = self._generate_overview_info(report_summary)
        
        for line in summary_list:
            # write one row of the table
            TABLE_CONTENT_HERE += '<tr style="height: 25px;">'
            for e in line:
                TABLE_CONTENT_HERE += '<td>' + e + '</td>'
                
            TABLE_CONTENT_HERE += '</tr>'
            

        with open(result_file_path, 'w') as result_file:
            with open(os.path.join(os.path.dirname(__file__), 'report_template.html'),'r') as report_template_file:
                report_template = report_template_file.read()

                report_template = report_template.replace('TABLE_CONTENT_HERE',
                                                          TABLE_CONTENT_HERE)
                
            result_file.write(report_template)

        html_report.append({'path': result_file_path,
                            'name': os.path.basename(result_file_path),
                            'label': os.path.basename(result_file_path),
                            'description': 'HTML summary report for the Metagenome Bin QC App'})
        return html_report

    def _generate_overview_info(self, report_summary):
        """
        _generate_overview_info: generate overview information from binQc.pl report 
        """
        summary_list = []
        with open(report_summary,'r') as file:
            for line in file:
                line_list = line.split("\t")
                binID = line_list[0]
                summary_list.append(line_list)
                
        return summary_list

    def generate_report(self, params):
        """
        generate_report: generate summary report

        """
        log('Generating report')

        result_directory = params.get('tar_directory')
        output_files_list = self._generate_output_file_list(result_directory)
        output_files   = output_files_list[0]
        report_summary = output_files_list[1]
        output_html_files = self._generate_html_report(report_summary)

        report_params = {
              'message': 'The summary of results can be viewed by the "Bin QC Results" table under "Report". However, if you want to take a look at the raw checkm results and the non-coding RNA annotation results, you can download them by clicking on "binqc_result.zip" under "Files".',
              'workspace_name': params.get('workspace_name'),
              'file_links': output_files,
              'html_links': output_html_files,
              'direct_html_link_index': 0,
              'html_window_height': 266,
              'report_object_name': 'binqc_report_' + str(uuid.uuid4())}
        

        kbase_report_client = KBaseReport(self.callback_url)
        output = kbase_report_client.create_extended_report(report_params)
        report_output = {'report_name': output['name'], 'report_ref': output['ref']}

        return report_output

    
