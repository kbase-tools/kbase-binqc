import json
import os
import uuid
import errno
import subprocess
import sys
import time
import zipfile
import shutil
from pprint import pprint
from biokbase.workspace.client import Workspace as workspaceService
from ConfigParser import ConfigParser
from DataFileUtil.DataFileUtilClient import DataFileUtil
from KBaseReport.KBaseReportClient import KBaseReport
from AssemblyUtil.AssemblyUtilClient import AssemblyUtil
from MetagenomeUtils.MetagenomeUtilsClient import MetagenomeUtils
from binqc.Utils.DataStagingUtils import DataStagingUtils
from binqc.Utils.MyReportUtils import CreateMyReport

# from kb_Msuite.kb_MsuiteClient import kb_Msuite
# import kb_Msuite.kb_MsuiteClient

def log(message, prefix_newline=False):
    """Logging function, provides a hook to suppress or redirect log messages."""
    print(('\n' if prefix_newline else '') + '{0:.2f}'.format(time.time()) + ': ' + str(message))
    
class binqc_runner:
    '''
    Module Name:
    binqc

    Module Description:
    A KBase module: binqc
    '''

    ######## WARNING FOR GEVENT USERS ####### noqa
    # Since asynchronous IO can lead to methods - even the same method -
    # interrupting each other, you must be *very* careful when using global
    # state. A method could easily clobber the state set by another while
    # the latter method is running.
    ######################################### noqa
    VERSION = "1.0.18"
    GIT_URL = "https://gitlab.com/kbase-tools/kbase-binqc"
    GIT_COMMIT_HASH = ""
    BINQC_TOOLKIT_PATH = '/kb/module/metaqc'
    BINQC_RESULTS_DIR = 'BinQC_results'
    BINQC_INPUT_DIR = 'Bins'
    THREADS=4
    CHECKM_SUFFIX = 'fa'
    
    def _validate_run_binqc_params(self, params):
        """
        _validate_run_binqc_params:
                validates params passed to bin_qc_util method

        """
        log('Start validating bin_qc_util params')

        # check for required parameters
        for p in ['binned_contig_obj_ref', 'workspace_name']:
            if p not in params:
                raise ValueError('"{}" parameter is required, but missing'.format(p))
     
    def _mkdir_p(self, path):
        """
        _mkdir_p: make directory for given path
        """
        if not path:
            return
        try:
            os.makedirs(path)
        except OSError as exc:
            if exc.errno == errno.EEXIST and os.path.isdir(path):
                pass
            else:
                raise
    
    def _get_bins_dir(self, binned_contig_obj_ref):
        """
        _get_contig_file: get contig file from GenomeAssembly object
        """        
        bin_dir = self.mgu.binned_contigs_to_file({'input_ref':binned_contig_obj_ref, 'save_to_shock': False})['bin_file_directory']
        assert (os.listdir(bin_dir) != []),'Bin directory is empty:\n{}'.format(bin_dir)        
        return bin_dir
    
    def _run_command(self, command):
        """
        _run_command: run command and print result
        """

        log('Start executing command:\n{}'.format(command))
        pipe = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
        output = pipe.communicate()[0]
        exitCode = pipe.returncode

        if (exitCode == 0):
            log('Executed command:\n{}\n'.format(command) +
                'Exit Code: {}\nOutput:\n{}'.format(exitCode, output))
        else:
            error_msg = 'Error running command:\n{}\n'.format(command)
            error_msg += 'Exit Code: {}\nOutput:\n{}'.format(exitCode, output)
            raise ValueError(error_msg)
        return output
    
    def _generate_command(self, bins_dir):
        """
        _generate_command: binQc.pl
        """
        # we should be running from /kb/module/test
        # create a results directory for binQc.pl
        outdir = os.path.join(self.scratch,self.BINQC_RESULTS_DIR)
        self._mkdir_p(outdir)
            
        command = "/bin/bash "
        command += self.BINQC_TOOLKIT_PATH + '/metaQc_wrapper.sh '       
        command += '{} {} {}'.format(bins_dir,outdir,self.THREADS)
        log('Generated metaQc_wrapper.pl command: {}'.format(command))

        return command
    
    def __init__(self, config, ctx):
        # Any configuration parameters that are important should be parsed and
        # saved in the constructor.
        self.ctx = ctx
        self.config = config
        self.scratch = config['scratch']
        self.callback_url = os.environ['SDK_CALLBACK_URL']
        self.mgu = MetagenomeUtils(self.callback_url)
        
    def bin_qc_util(self, params):
        """
        The actual function is declared using 'funcdef' to specify the name
        and input/return arguments to the function.  For all typical KBase
        Apps that run in the Narrative, your function should have the 
        'authentication required' modifier.
        :param params: instance of type "BinQCParams" (A 'typedef' can also
           be used to define compound or container objects, like lists, maps,
           and structures.  The standard KBase convention is to use
           structures, as shown here, to define the input and output of your
           function.  Here the input is a reference to the binned contigs
           data object and a workspace to save output. To define lists and
           maps, use a syntax similar to C++ templates to indicate the type
           contained in the list or map.  For example: list <string>
           list_of_strings; mapping <string, int> map_of_ints;) -> structure:
           parameter "binned_contig_obj_ref" of type "obj_ref" (An X/Y/Z
           style reference), parameter "workspace_name" of String
        :returns: instance of type "BinQCResults" (Here is the definition of
           the output of the function.  The output can be used by other SDK
           modules which call your code, or the output visualizations in the
           Narrative.  'report_name' and 'report_ref' are special output
           fields- if defined, the Narrative can automatically render your
           Report.) -> structure: parameter "report_name" of String,
           parameter "report_ref" of String
        """
        # ctx is the context object
        # return variables are: output
        
        # validate that we have all required params
        self._validate_run_binqc_params(params)
        
        # test what object type we have as input and extract the assembly_ref
        # for example test if we have KBaseGenomeAnnotations.Assembly or KBaseGenomes.Genome
        dsu = DataStagingUtils(self.config, self.ctx)
        staged_fasta_files = dsu.stage_input(params['binned_contig_obj_ref'])
        # print "STAGED FILE: {}".format(staged_fasta_files)
        
        # move assemblies (bins & genomes) into some directory that we
        # can then pass to binQc.pl
        bins_dir = os.path.join(self.scratch,self.BINQC_INPUT_DIR)
        self._mkdir_p(bins_dir)
        for assembly in staged_fasta_files:
            bname = os.path.basename(assembly)
            if not os.path.exists(bins_dir +'/'+ bname):
                shutil.move(assembly, bins_dir + '/' + bname)
       
        checkm_data = '/data/checkm_data'
        if not os.path.isdir(checkm_data):
            log("ref data for checkm not found: {}".format(checkm_data))
            sys.exit()
       
        #
        # run binQc_wrapper.sh
        #
        log('running checkm...')
        command = self._generate_command(bins_dir)
        self._run_command(command)
        
        #########################
        # now for the report
        #########################
        result_directory = os.path.join(self.scratch,self.BINQC_RESULTS_DIR)
        params['tar_directory'] = result_directory
        
        rep = CreateMyReport(self.config, self.ctx)
        returnVal = rep.generate_report(params)
                
        returnParams = {
            'result_directory': result_directory,
        }
        returnVal.update(returnParams)
        
        # At some point might do deeper type checking...
        if not isinstance(returnVal, dict):
            raise ValueError('Method bin_qc return value ' +
                             'output is not type dict as required.')
        
        return returnVal
    
    
